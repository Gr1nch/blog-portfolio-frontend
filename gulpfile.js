"use strict";

const gulp = require('gulp')
    , connect = require('gulp-connect')
    , pug = require('gulp-pug')
    , stylus = require('gulp-stylus')
    , babelify = require('babelify')
    , browserify = require('browserify')
    , pugify = require('pugify')
    , source = require('vinyl-source-stream');

const outDir = __dirname + '/static';

gulp.task('pug', function() {
  return gulp.src(['src/app/**/*.pug', '!src/index.pug'])
    .pipe(pug().on('error', err => {
      console.log(err);
    }))
    .pipe(gulp.dest(outDir + '/html'));
});

gulp.task('stylus', function() {
  return gulp.src(['src/assets/stylus/**/*.styl', 'src/app/common/directives/**/*.styl'])
    .pipe(stylus({
      compress: true,
      'include css': true
    }))
    .pipe(gulp.dest(outDir + '/css'));
});

gulp.task('index', function() {
  return gulp.src('src/index.pug')
    .pipe(pug().on('error', err => {
      console.log(err);
    }))
    .pipe(gulp.dest(outDir));
});

gulp.task('js', function() {
  browserify({
    entries: 'src/app/app.js'
  }).on('error', err => {
    console.log(err);
  })
    .transform(pugify.pug({pretty: false}))
    .transform(babelify.configure({
      presets: 'es2015'
    })).on('error', err => {
      console.log(err);
    })
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(gulp.dest(outDir + '/js'));
  // .pipe(connect.reload());
});

gulp.task('admin-js', function() {
  browserify({
    entries: 'src/app/admin.js'
  }).on('error', err => {
    console.log(err);
  })
    .transform(pugify.pug({pretty: false}))
    .transform(babelify.configure({
      presets: 'es2015'
    })).on('error', err => {
      console.log(err);
    })
    .bundle()
    .pipe(source('admin.bundle.js'))
    .pipe(gulp.dest(outDir + '/js'));
  // .pipe(connect.reload());
});

gulp.task('connect', function() {
  connect.server({
    root: outDir,
    fallback: outDir + '/index.html',
    port: '3000'
  });
});

gulp.task('font', function() {
  return gulp.src('node_modules/font-awesome/fonts/*.{ttf,otf,eot,svg,woff,woff2}')
    .pipe(gulp.dest(outDir + '/fonts'));
});

gulp.task('watch', function() {
  gulp.watch(['src/app/**/*.pug'], ['pug']);
  gulp.watch(['src/index.pug'], ['index']);
  gulp.watch(['src/app/main/**/*.js', 'src/app/common/**/*.js', 'src/app/app.js'], ['js']);
  gulp.watch(['src/app/admin/**/*.js', 'src/app/common/**/*.js', 'src/app/admin.js'], ['admin-js']);
  gulp.watch(['src/assets/stylus/**/*.styl', 'src/app/common/directives/**/*.styl'], ['stylus']);
});

gulp.task('build', ['pug', 'stylus', 'index', 'js', 'admin-js', 'font'], function(done) {
  done();
});

gulp.task('default', ['build', 'connect', 'watch']);
