"use strict";

function headerController(
  $scope,
  $state,
  authService
) {
  let vm = this;
  vm.auth = false;

  init();
  function init() {
    if (authService.getToken()) {
      vm.auth = true;
    }

    $scope.$on('setToken', (event, data) => {
      vm.auth = true;
    });
  }

  vm.logout = function() {
    authService.logout();
    vm.auth = false;
    $state.go('login');
  };
}

module.exports = angular
  .module('admin')
  .controller('headerController', headerController)
  .name;
