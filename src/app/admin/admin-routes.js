"use strict";

module.exports = function(
  $stateProvider,
  appConfig
) {
  $stateProvider
    .state('init', {
      abstract: true,
      views: {
        '': {
          templateUrl: `${appConfig.tmpUrl}/admin/init/init.view.html`,
          controller: 'initController',
          controllerAs: '$init'
        },
        'content@init': {
          template: `<ui-view></ui-view>`,
          controller: function() {}
        },
        'header@init': {
          templateUrl: `${appConfig.tmpUrl}/admin/header/header.view.html`,
          controller: 'headerController',
          controllerAs: '$header'
        }
      }
    })
    .state('main', {
      parent: 'init',
      url: '/',
      templateUrl: `${appConfig.tmpUrl}/admin/main/main.view.html`,
      controller: 'mainController',
      controllerAs: '$main'
    })
    .state('login', {
      parent: 'init',
      url: '/login',
      templateUrl: `${appConfig.tmpUrl}/admin/login/login.view.html`,
      controller: 'loginController',
      controllerAs: '$login'
    })
    .state('topic', {
      parent: 'init',
      url: '/topic',
      templateUrl: `${appConfig.tmpUrl}/admin/topic/topic.view.html`,
      controller: 'topicController',
      controllerAs: '$topic',
      resolve: {
        topics: function(
          topicService,
          $q
        ) {
          let defer = $q.defer();
          topicService.getList()
            .then(topics => {
              _.each(topics, item => {
                item.createdFormat = moment(item.created).format('DD.MM.YYYY HH:mm');
              })
              defer.resolve(topics);
              })
            .catch(rej => {
              defer.reject(rej);
            });
          return defer.promise;
        }
      }
    })
}