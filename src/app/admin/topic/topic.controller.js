'use strict';

function topicController(
  $scope,
  topics,
  topicService,
  modalService,
  alertService
) {
  let vm = this;
  vm.topics = topics;

  init();
  function init() {

  }

  vm.openCreate = function() {
    modalService.open(createTopic, '/admin/topic/create-topic.modal.html')
      .result.then(res => {
        console.log(res);
        res.createdFormat = moment(res.created).format('DD.MM.YYYY HH:mm');
        vm.topics.push(res);
        alertService.success('Add new topic success');
      });
  };
}

function createTopic(
  $uibModalInstance,
  topicService
) {
  let vm = this;
  vm.data = {
    title: '',
    description: ''
  };

  vm.ok = function() {
    topicService.new(vm.data)
      .then(res => {
        $uibModalInstance.close(res);
      })
      .catch(rej => {
        vm.error = rej;
      })
  };

  vm.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  }

}

module.exports = angular
  .module('admin')
  .controller('topicController', topicController)
  .name;
