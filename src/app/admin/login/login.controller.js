"use strict";

function loginController(
  $state,
  $scope,
  authService,
  alertService
) {
  console.log('login controller');
  let vm = this;

  vm.login = function() {
    authService.login(vm.name, vm.password)
      .then(res => {
        alertService.success('Authorization success');
        $state.go('main');
      })
      .catch(rej => {
        alertService.error(rej.message);
      });
  };
}

module.exports = angular
  .module('admin')
  .controller('loginController', loginController)
  .name;
