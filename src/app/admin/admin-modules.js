"use strict";

const routes = require('./admin-routes')
    , appConfig = require('./../config');

module.exports = angular.module('admin', [])
  .constant('appConfig', appConfig)
  .config(routes)
  .name;

require('./init/init.controller');
require('./login/login.controller');
require('./header/header.controller');
require('./main/main.controller');
require('./topic/topic.controller');
