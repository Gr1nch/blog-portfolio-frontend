'use strict';

function config($stateProvider, $urlRouterProvider, $locationProvider, $qProvider, appConfig) {
  $qProvider.errorOnUnhandledRejections(false);
  $urlRouterProvider.otherwise('/');
  $locationProvider.html5Mode(true);
}

module.exports = config;
