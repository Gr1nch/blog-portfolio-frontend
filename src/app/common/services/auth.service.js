"use strict";

function authService(
  $http,
  $q,
  $rootScope,
  apiService
) {
  this._token = null;

  this.getToken = function() {
    return this._token || localStorage.getItem('authToken');
  };

  this.setToken = function(token) {
    this._token = token;
    localStorage.setItem('authToken', token);
    $rootScope.$broadcast('setToken', true);
  };

  this.login = function(login, password) {
    let base = 'Basic ' + window.btoa(login + ':' + password);
    $http.defaults.headers.common.Authorization = base;
    return apiService.call('GET', '/user/login')
      .then(() => {
        this.setToken(base);
      })
  };

  this.logout = function() {
    this._token = null;
    localStorage.removeItem('authToken');
    $http.defaults.headers.common.Authorization = '';
  };

  this.relogin = function() {
    let defer = $q.defer();
    if (this.getToken()) {
      $http.defaults.headers.common.Authorization = this.getToken();
      defer.resolve(true);
    } else {
      defer.reject(false);
    }
    return defer.promise;
  };

  return this;
}

module.exports = angular
  .module('common')
  .service('authService', authService)
  .name;
