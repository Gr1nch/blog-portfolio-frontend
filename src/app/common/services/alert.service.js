"use strict";

function alertService(
  $rootScope
) {

  function emit(msg, type, dismiss) {
    let data = {msg, type};
    if (dismiss) data.dismiss = dismiss;
    $rootScope.$broadcast('addAlert', data);
  }

  this.success = function(msg, dismiss) {
    emit(msg, 'success', dismiss);
  };

  this.error = function(msg, dismiss) {
    emit(msg, 'danger', dismiss);
  };

  this.warning = function(msg, dismiss) {
    emit(msg, 'warning', dismiss);
  };

  this.info = function(msg, dismiss) {
    emit(msg, 'info', dismiss);
  };

  return this;
}

module.exports = angular
  .module('common')
  .service('alertService', alertService)
  .name;