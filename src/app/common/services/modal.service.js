"use strict";

function modalService(
  $uibModal,
  appConfig
) {
  this.instance = null;

  this.open = function(ctrl, tmp, resolve) {
    this.instance = $uibModal.open({
      animate: true,
      controller: ctrl,
      controllerAs: '$mdl',
      templateUrl: `${appConfig.tmpUrl}${tmp}`,
      resolve: resolve
    });

    return this.instance;
  };

  return this;
}

module.exports = angular
  .module('common')
  .service('modalService', modalService)
  .name;