"use strict";

function topicService(
  apiService
) {

  this.getList = function() {
    return apiService.call('GET', `/topic`);
  };

  this.getById = function(id) {
    return apiService.call('GET', `/topic/${id}`);
  };

  this.new = function(data) {
    return apiService.call('POST', `/topic`, data);
  };

  this.deleteById = function(id) {
    return apiService.call('DELETE', `/topic/${id}`);
  };

  this.updateById = function(id, data) {
    return apiService.call('PUT', `/topic/${id}`, data);
  };

  return this;
}

module.exports = angular
  .module('common')
  .service('topicService', topicService)
  .name;