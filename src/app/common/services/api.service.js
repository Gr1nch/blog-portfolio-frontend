"use strict";

function apiService(
  appConfig,
  $http,
  $q
) {
  this.call = function(method, url, data) {
    let prepare = {
        method: method,
        url: `${appConfig.api}${url}`
      }
      , defer = $q.defer();

    if (data) {
      prepare.data = data;
    }
    $http(prepare)
      .then(res => {
        if (res.data.success) {
          defer.resolve(res.data.data);
        } else {
          defer.reject(res.data.error);
        }
      })
      .catch(rej => {
        defer.reject(rej);
      });

    return defer.promise;
  };

  return this;
}

module.exports = angular
  .module('common')
  .service('apiService', apiService)
  .name;