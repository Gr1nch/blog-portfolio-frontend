'use strict';

function alert(
  appConfig
) {
  return {
    restrict: 'E',
    template: require('./alert.view.pug'),
    link: function(scope) {
      scope.alerts = [];
      scope.$on('addAlert', (event, data) => {
        scope.alerts.push({
          type: data.type,
          msg: data.msg,
          dismiss: 5000
        });
      });

      scope.closeAlert = function(index) {
        scope.alerts.splice(index, 1);
      };
    }
  }
}

module.exports = angular
  .module('common')
  .directive('alert', alert)
  .name;
