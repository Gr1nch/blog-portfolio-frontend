"use strict";

const appConfig = require('./../config');

module.exports = angular.module('common', [])
  .constant('appConfig', appConfig)
  .name;

// Services
require('./services/api.service');
require('./services/auth.service');
require('./services/topic.service');
require('./services/modal.service');
require('./services/alert.service');

// Directives
require('./directives/alert/alert');
