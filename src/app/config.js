'use strict';

const appConfig = {
  tmpUrl: 'html',
  api: 'http://localhost:1337/api'
};

module.exports = appConfig;