"use strict";

window._ = require('lodash');
window.moment = require('moment');
moment.locale('ru');
const angular = require('angular')
    , uiRouter = require('angular-ui-router')
    , ngBootstrap = require('angular-ui-bootstrap')
    , ngAnimate = require('angular-animate')
    , common = require('./common/common-modules')
    , admin = require('./admin/admin-modules')
    , router = require('./router');

angular.module('app', [
  uiRouter,
  ngBootstrap,
  ngAnimate,
  common,
  admin
])
.config(router)
.run((
  $rootScope,
  $state,
  authService
) => {
  console.log('run');
  $rootScope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams, options) => {
    authService.relogin()
      .then(() => {
        if (toState.name === 'login') {
          event.preventDefault();
          $state.go('main');
        }
      })
      .catch(() => {
        if (toState.name !== 'login') {
          event.preventDefault();
          $state.go('login');
        }
      })
  });
});
